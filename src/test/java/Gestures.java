import io.appium.java_client.TouchAction;
import io.appium.java_client.android.AndroidDriver;
import io.appium.java_client.android.AndroidElement;
import io.appium.java_client.touch.LongPressOptions;
import io.appium.java_client.touch.TapOptions;
import io.appium.java_client.touch.offset.ElementOption;
import org.openqa.selenium.WebElement;

import java.net.MalformedURLException;
import java.time.Duration;

public class Gestures extends Base {

    public static void main(String[] args) throws MalformedURLException {

        AndroidDriver<AndroidElement> driver = capabilities();

        driver.findElementByXPath("//android.widget.TextView[@text='Views']").click();
        //tap on sth
        TouchAction touch = new TouchAction<>(driver);
        WebElement element = driver.findElementByXPath("//android.widget.TextView[@text='Expandable Lists']");
        touch.tap(TapOptions.tapOptions().withElement(ElementOption.element(element))).perform();
        touch.tap(TapOptions.tapOptions().withElement(ElementOption.element(driver.findElementByXPath("//android.widget.TextView[@text='1. Custom Adapter']")))).perform();
        touch.longPress(LongPressOptions.longPressOptions()
                .withElement(ElementOption.element(driver.findElementByXPath("//android.widget.TextView[@text='People Names']"))).withDuration(Duration.ofSeconds(2)))
                .release()
                .perform();
        System.out.println(driver.findElementById("android:id/title").isDisplayed());
    }
}
