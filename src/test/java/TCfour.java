import com.google.common.collect.ImmutableMap;
import io.appium.java_client.TouchAction;
import io.appium.java_client.android.AndroidDriver;
import io.appium.java_client.android.AndroidElement;
import io.appium.java_client.android.nativekey.AndroidKey;
import io.appium.java_client.android.nativekey.KeyEvent;
import io.appium.java_client.service.local.AppiumDriverLocalService;
import io.appium.java_client.touch.TapOptions;
import io.appium.java_client.touch.offset.ElementOption;

import java.io.IOException;
import java.net.ServerSocket;
import java.util.Set;
import java.util.concurrent.TimeUnit;

public class TCfour extends Base{

    AppiumDriverLocalService service;

    public static void main(String[] args) throws IOException, InterruptedException {


        TCfour tCfour = new TCfour();

        tCfour.killAllNodes();
        tCfour.startEmulator();

        System.out.println(tCfour.isServerRunning(4723));
        if(!tCfour.isServerRunning(4723)) {
            tCfour.service = AppiumDriverLocalService.buildDefaultService();
            tCfour.service.start();
            System.out.println("starting server");
        }

        AndroidDriver<AndroidElement> driver = capabilities();
        driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);

        driver.findElementById("com.androidsample.generalstore:id/nameField").sendKeys("Marcin");
        driver.hideKeyboard();
        driver.findElementById("com.androidsample.generalstore:id/btnLetsShop").click();
        driver.findElementsByXPath("//*[@text='ADD TO CART']").get(0).click();
        driver.findElementById("com.androidsample.generalstore:id/appbar_btn_cart").click();
        Thread.sleep(2000);

        TouchAction touch = new TouchAction(driver);
        touch.tap(TapOptions.tapOptions()
                .withElement(ElementOption.element(driver.findElementByClassName("android.widget.CheckBox")))).perform();

        driver.findElementById("com.androidsample.generalstore:id/btnProceed").click();
        Thread.sleep(2000);
        
        Set<String> contexts = driver.getContextHandles();
        for (String context:contexts) {
            System.out.println(context);
        }
        driver.context("WEBVIEW_com.androidsample.generalstore");
        driver.findElementByXPath("//input[@name='q']").sendKeys("marcin");
        driver.pressKey(new KeyEvent(AndroidKey.BACK));
        driver.context("NATIVE_APP");
        //Runtime.getRuntime().exec("adb -s Pixel_3 emu kill");
        //driver.executeScript("mobile: execEmuConsoleCommand", ImmutableMap.of("command","kill"));
        tCfour.service.stop();

    }

    public boolean isServerRunning(int port) {
        boolean isRunning = false;
        ServerSocket serverSocket;
        try {
            serverSocket = new ServerSocket(port);
            serverSocket.close();
        } catch (IOException e) {
            isRunning = true;
        } finally {
            serverSocket = null;
        }
        return isRunning;
    }

    public void startEmulator() throws IOException, InterruptedException {
        Runtime.getRuntime().exec("src/test/resources/start_emu.bat");
        System.out.println("starting emu");
        Thread.sleep(5000);
    }

    public void killAllNodes() throws IOException {
        Runtime.getRuntime().exec("taskkill /F /IM node.exe");
    }
}
