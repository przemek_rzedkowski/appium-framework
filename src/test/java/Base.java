import io.appium.java_client.android.AndroidDriver;
import io.appium.java_client.android.AndroidElement;
import io.appium.java_client.remote.MobileCapabilityType;
import org.openqa.selenium.remote.DesiredCapabilities;

import java.io.File;
import java.net.MalformedURLException;
import java.net.URL;

public class Base {

    public static AndroidDriver<AndroidElement> capabilities() throws MalformedURLException {

        File file = new File("src");
        //File ap_file = new File(file, "ApiDemos-debug.apk");
        File ap_file = new File(file, "General-Store.apk");

        DesiredCapabilities caps = new DesiredCapabilities();
        caps.setCapability(MobileCapabilityType.DEVICE_NAME, "Pixel_3");
        caps.setCapability(MobileCapabilityType.APP, ap_file.getAbsolutePath());
        caps.setCapability(MobileCapabilityType.AUTOMATION_NAME,"uiautomator2");
        caps.setCapability(MobileCapabilityType.NEW_COMMAND_TIMEOUT, 10);
        caps.setCapability("autoGrantPermissions",true);
        caps.setCapability(MobileCapabilityType.VERSION, "10.0");
        caps.setCapability("noReset", "true");
        caps.setCapability("fullReset", "false");
        caps.setCapability(MobileCapabilityType.PLATFORM_NAME, "Android");

        return new AndroidDriver<AndroidElement>(new URL("http://localhost:4723/wd/hub"), caps);
    }
}
