import io.appium.java_client.MobileBy;
import io.appium.java_client.android.AndroidDriver;
import io.appium.java_client.android.AndroidElement;
import org.junit.Assert;

import java.net.MalformedURLException;
import java.util.concurrent.TimeUnit;

public class TCtwo extends Base {

    public static void main(String[] args) throws MalformedURLException {

        AndroidDriver<AndroidElement> driver = capabilities();
        driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);

        driver.findElementById("com.androidsample.generalstore:id/nameField").sendKeys("Marcin");
        driver.hideKeyboard();
        driver.findElementById("com.androidsample.generalstore:id/btnLetsShop").click();

        driver.findElement(MobileBy.AndroidUIAutomator("new UiScrollable(new UiSelector().resourceId(\"com.androidsample.generalstore:id/rvProductList\"))" +
                ".scrollIntoView(new UiSelector().textMatches(\"Converse All Star\").instance(0))"));

        int count = driver.findElementsById("com.androidsample.generalstore:id/productName").size();
        String name;

        for (int i = 0; i < count; i++) {
            name = driver.findElementsById("com.androidsample.generalstore:id/productName").get(i).getText();
            System.out.println(name);
            if(name.equals("Converse All Star")) {
                driver.findElementsById("com.androidsample.generalstore:id/productAddCart").get(i).click();
            }
        }

        driver.findElementById("com.androidsample.generalstore:id/appbar_btn_cart").click();
        String artykul = driver.findElementById("com.androidsample.generalstore:id/productName").getText();
        Assert.assertEquals("Converse All Star", artykul);
    }
}
