import io.appium.java_client.TouchAction;
import io.appium.java_client.android.AndroidDriver;
import io.appium.java_client.android.AndroidElement;
import io.appium.java_client.touch.LongPressOptions;
import io.appium.java_client.touch.offset.ElementOption;

import java.net.MalformedURLException;
import java.time.Duration;

public class Swipe extends Base {

    public static void main(String[] args) throws MalformedURLException {

        AndroidDriver<AndroidElement> driver = capabilities();

        driver.findElementByXPath("//android.widget.TextView[@text='Views']").click();
        driver.findElementByXPath("//android.widget.TextView[@text='Date Widgets']").click();
        driver.findElementByXPath("//android.widget.TextView[@text='2. Inline']").click();
        driver.findElementByXPath("//*[@content-desc='9']").click();
        TouchAction touch = new TouchAction<>(driver);
        touch.longPress(LongPressOptions.longPressOptions().withElement(ElementOption.element(driver.findElementByXPath("//*[@content-desc='15']")))
                .withDuration(Duration.ofSeconds(1)))
                .moveTo(ElementOption.element(driver.findElementByXPath("//*[@content-desc='40']")))
                .release()
                .perform();

    }
}
