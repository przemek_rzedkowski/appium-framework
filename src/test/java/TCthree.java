import io.appium.java_client.TouchAction;
import io.appium.java_client.android.AndroidDriver;
import io.appium.java_client.android.AndroidElement;
import io.appium.java_client.touch.LongPressOptions;
import io.appium.java_client.touch.TapOptions;
import io.appium.java_client.touch.offset.ElementOption;
import org.junit.Assert;

import java.net.MalformedURLException;
import java.time.Duration;
import java.util.List;
import java.util.concurrent.TimeUnit;

public class TCthree extends Base {

    public static void main(String[] args) throws MalformedURLException, InterruptedException {

        TCthree tCthree = new TCthree();
        AndroidDriver<AndroidElement> driver = capabilities();
        driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);

        driver.findElementById("com.androidsample.generalstore:id/nameField").sendKeys("Marcin");
        driver.hideKeyboard();
        driver.findElementById("com.androidsample.generalstore:id/btnLetsShop").click();

        List<AndroidElement> list = driver.findElementsByXPath("//*[@text='ADD TO CART']");

        for (AndroidElement e: list) {
            e.click();
        }

        driver.findElementById("com.androidsample.generalstore:id/appbar_btn_cart").click();
        Thread.sleep(2000);
        String price_a = driver.findElementsById("com.androidsample.generalstore:id/productPrice").get(0).getText();
        String price_b = driver.findElementsById("com.androidsample.generalstore:id/productPrice").get(1).getText();
        String price_sum = "$ " + Double.toString( tCthree.getAmount(price_a) + tCthree.getAmount(price_b));
        String app_sum = driver.findElementById("com.androidsample.generalstore:id/totalAmountLbl").getText();
        Assert.assertEquals(price_sum, app_sum);

        TouchAction touch = new TouchAction(driver);
        touch.tap(TapOptions.tapOptions()
                .withElement(ElementOption.element(driver.findElementByClassName("android.widget.CheckBox")))).perform();
        touch.longPress(LongPressOptions.longPressOptions()
                .withElement(ElementOption.element(driver.findElementById("com.androidsample.generalstore:id/termsButton")))
                .withDuration(Duration.ofSeconds(2)))
                .release()
                .perform();
        driver.findElementById("android:id/button1").click();
        driver.findElementById("com.androidsample.generalstore:id/btnProceed").click();
    }

    public double getAmount(String value) {
        value = value.substring(1);
        return Double.parseDouble(value);
    }
}
